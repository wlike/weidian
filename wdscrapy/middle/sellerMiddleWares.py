# -*- coding: utf-8 -*-

# Define here the models for your spider middleware
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/spider-middleware.html
import random
from scrapy import signals
import sys
import io
import time
from selenium import webdriver


class RandomUserAgent(object):

    def __init__(self, agents):
        self.agents = agents

    @classmethod
    def from_crawler(cls, crawler):
        return cls(crawler.settings.getlist('USER_AGENTS'))

    def process_request(self, request, spider):
        print("**************************" + random.choice(self.agents))

        referer = request.url
        if referer:
            request.headers["referer"] = referer
        request.headers.setdefault('User-Agent', random.choice(self.agents))

        sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='utf8')  # 改变标准输出的默认编码

        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--disable-gpu')
        chrome_options.add_argument('--disable-dev-shm-usage')
        driver = webdriver.Chrome(chrome_options=chrome_options, executable_path='./chromedriver')

        def is_element_exist(css):
            s = driver.find_elements_by_css_selector(css_selector=css)
            if len(s) == 0:
                print("元素未找到:%s" % css)
                return False
            elif len(s) == 1:
                return True
            else:
                print("找到%s个元素：%s" % (len(s), css))
                return False

        driver.get(referer)

        driver.find_element_by_xpath("//input[@placeholder='手机号']").send_keys(spider.user)
        driver.find_element_by_xpath("//input[@placeholder='输入登录密码']").send_keys(spider.user_pass)
        # driver.find_element_by_xpath("//input[@placeholder='手机号']").send_keys("13516720777")
        # driver.find_element_by_xpath("//input[@placeholder='输入登录密码']").send_keys("hdd36000")

        driver.find_element_by_class_name('weidian-button').click()
        time.sleep(1)

        driver.find_element_by_class_name('shop_name').click()
        time.sleep(1)

        driver.get("https://d.weidian.com/weidian-pc/pc-vue-order/#/orderList?listType=1")
        time.sleep(3)

        driver.find_element_by_xpath("//input[@placeholder='请输入订单编号']").send_keys(spider.orderid)
        # driver.find_element_by_xpath("//input[@placeholder='请输入订单编号']").send_keys("812645220812635")
        driver.find_element_by_xpath("//span[contains(text(),'筛选')]").click()
        time.sleep(0.5)

        driver.find_element_by_xpath("//div[@class='operation-info']//div[@class='list']//a[contains(text(),'发货')]").click()
        time.sleep(1.5)

        driver.find_element_by_xpath("//input[@placeholder='请输入快递单号']").send_keys(spider.expressno)
        # driver.find_element_by_xpath("//input[@placeholder='请输入快递单号']").send_keys("234235423142342")
        time.sleep(1)

        driver.find_element_by_xpath("//span[contains(text(),'发 货')]").click()
        time.sleep(0.2)

        driver.quit()
