# -*- coding: utf-8 -*-

# Define here the models for your spider middleware
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/spider-middleware.html
import random
import sys
import io
import re
import base64
import traceback
from PIL import Image
from pyzbar import pyzbar
from selenium.webdriver import DesiredCapabilities
import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

class RandomUserAgent(object):

    def __init__(self, agents):
        self.agents = agents

    @classmethod
    def from_crawler(cls, crawler):
        return cls(crawler.settings.getlist('USER_AGENTS'))

    def process_request(self, request, spider):
        # print("**************************" + random.choice(self.agents))
        user_agent  = random.choice(self.agents)

        referer = request.url
        if referer:
            request.headers["referer"] = referer
        request.headers.setdefault('User-Agent', random.choice(self.agents))

        # sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='utf8')  # 改变标准输出的默认编码

        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--disable-gpu')
        chrome_options.add_argument('--disable-dev-shm-usage')
        driver = webdriver.Chrome(chrome_options=chrome_options, executable_path='./chromedriver')
        # driver = webdriver.Chrome(chrome_options=chrome_options)

#        dcap = dict(DesiredCapabilities.PHANTOMJS)
#        dcap["phantomjs.page.settings.userAgent"] = user_agent
#        dcap["phantomjs.page.customHeaders.User-Agent"] = user_agent
#        driver = webdriver.PhantomJS(executable_path='./phantomjs', desired_capabilities=dcap)

        def is_element_exist(css):
            s = driver.find_elements_by_css_selector(css_selector=css)
            if len(s) == 0:
                print("元素未找到:%s" % css)
                return False
            elif len(s) == 1:
                return True
            else:
                print("找到%s个元素：%s" % (len(s), css))
                return False
                
        def is_xpath_element_exist(xp):
            try:
                print("查询支付宝是否存在")
                driver.find_element_by_xpath(xp)
                print("支付宝存在")
                return True
            except Exception:
                print("支付宝不存在")
                return False

        driver.get(referer)

        driver.find_element_by_id('login_init_by_login').click()
        time.sleep(0.5)

        driver.find_element_by_class_name('login_content_h4').click()

        driver.find_element_by_id('login_isRegiTele_input').send_keys(spider.user)
        driver.find_element_by_id('login_pwd_input').send_keys(spider.user_pass)

        driver.find_element_by_id('login_pwd_submit').click()
        time.sleep(0.5)

        driver.get(spider.good_url)
        time.sleep(1)

        if is_element_exist("#_confirm_btnB"):
            driver.find_element_by_id("_confirm_btnB").click()
            time.sleep(2)

        driver.find_element_by_class_name('buy-now').click()
        time.sleep(0.5)

        if is_element_exist("#_confirm_btnB"):
            driver.find_element_by_id("_confirm_btnB").click()
            time.sleep(2)

        if is_element_exist(".e-active-newuser-coupon-close"):
            driver.find_element_by_class_name('e-active-newuser-coupon-close').click()

        if is_element_exist(".footer-buy-now"):
            driver.find_element_by_class_name('footer-buy-now').click()
            time.sleep(0.3)

        # driver.find_element_by_id('submit_order').click()
        driver.find_element_by_xpath("//a[contains(text(),'提交订单') and contains(@class,'submit_order')]").click()

        if is_element_exist("#_confirm_btnB"):
            driver.find_element_by_id("_confirm_btnB").click()

        time.sleep(3)
        qrcode = driver.find_element_by_id('qrCode').get_attribute('src')

        base64_data = re.sub('^data:image/.+;base64,', '', qrcode).replace("%0A", "")

        # base64解码
        imagedata = base64.b64decode(base64_data)

        # 转二进制
        image_data = io.BytesIO(imagedata)

        # 图片读取
        img = Image.open(image_data)

        # 二维码图片解析
        txt_list = pyzbar.decode(img)

        # 遍历解析结果
        for txt in txt_list:
            barcodeData = txt.data.decode("utf-8")
            print('二维码地址=' + barcodeData)

        driver.get(barcodeData)
        time.sleep(3)
        if is_element_exist("#_confirm_btnB"):
            driver.find_element_by_id("_confirm_btnB").click()
            time.sleep(2)

        if is_xpath_element_exist("//div[@class='close']"):
            driver.find_element_by_xpath("//div[@class='close']").click()


        print("执行JS")
        orderNojs = "return eval('(' + sessionStorage.getItem('vuex') + ')').app.outOrderIds[0];"
        orderno = driver.execute_script(orderNojs)
        time.sleep(1)
        print("执行JS结束,休息一秒")

        if is_xpath_element_exist("//h3[contains(text(),'支付宝')]"):
            print("点击支付宝")
            driver.find_element_by_xpath("//h3[contains(text(),'支付宝')]/../..").click()
            print("点击支付宝完成1")
        else:
            print("未进入支付宝，查找更多支付方式")
            js = "var q=document.documentElement.scrollTop=10000"  # documentElement表示获取根节点元素
            driver.execute_script(js)
            time.sleep(3)
            try:
                WebDriverWait(driver, 6).until(
                    EC.presence_of_element_located((By.XPATH, "//span[contains(text(),'更多支付方式')]"))
                )
                driver.find_element_by_xpath("//span[contains(text(),'更多支付方式')]").click()

            except Exception as e:
                eles = driver.find_element_by_xpath("//span[contains(text(),'更多支付方式')]")
                err = None
                for i in eles:
                    if i.text.strip() != '':
                        err = i.text
                        print('错误信息 %s' % i.text)
                if not err:
                    err = traceback.format_exc()
            js = "var q=document.documentElement.scrollTop=10000"  # documentElement表示获取根节点元素
            driver.execute_script(js)
            time.sleep(0.5)
            driver.find_element_by_xpath("//h3[contains(text(),'支付宝')]/../..").click()

        print("点击支付宝完成2")
        time.sleep(2)
        # if is_xpathelement_exist("//span[contains(text(),'更多支付方式')]"):
        #     driver.find_element_by_xpath("//span[contains(text(),'更多支付方式')]").click()
        #
        # orderNojs = "return eval('(' + sessionStorage.getItem('vuex') + ')').app.outOrderIds[0];"
        # orderno = driver.execute_script(orderNojs)

        # js = "var q=document.documentElement.scrollTop=10000"  # documentElement表示获取根节点元素
        # driver.execute_script(js)
        # time.sleep(0.5)
        # driver.find_element_by_xpath("//h3[contains(text(),'支付宝')]/../..").click()
        print("点击立即支付：前")
        driver.find_element_by_id('payBtn').click()
        print("点击立即支付：后")
        time.sleep(2)

        for num in range(1, 5):
            aliurl = driver.current_url
            if "mclient.alipay.com" not in aliurl:
                time.sleep(2)
            else:
                break
        # current_url 方法可以得到当前页面的URL

        spider.ali_url = driver.current_url
        spider.wd_order_no = orderno
        driver.quit()

