# -*- coding: utf-8 -*-
import json
from scrapy.spiders import Spider
from urllib import parse
from scrapy.http import Request
import requests

class WeidianSpider(Spider):
    # 爬虫的名称
    name = 'weidian_scrapy'
    # 爬虫允许抓取的域名
    allowed_domains = ['sso.weidian.com']

    custom_settings = {
        'DOWNLOADER_MIDDLEWARES': {
            'scrapy.downloadermiddlewares.useragent.UserAgentMiddleware': None,
            'wdscrapy.middle.wedianmiddlewares.RandomUserAgent': 400,
        }
    }
    # 爬虫抓取数据地址,给调度器
    #start_urls = ['https://sso.weidian.com/user/login?countryCode=86&phone=18956119106&password=zyhero123&version=1']
    #start_urls = ['https://sso.weidian.com/login/index.php']
    oid = None
    good_url = None
    user = None
    user_pass = None
    ali_url = None
    wd_order_no = None

    def __init__(self, oid=None, good_url=None, user=None, user_pass=None, *args, **kwargs):
        super(eval(self.__class__.__name__), self).__init__(*args, **kwargs)
        self.oid = oid
        self.good_url = parse.unquote(good_url)
        self.user = user
        self.user_pass = user_pass
        #self.start_urls = ['https://sso.weidian.com/login/index.php']

    def start_requests(self):
        yield Request('https://sso.weidian.com/login/index.php', callback=self.parse_with_cookie)

    def parse_with_cookie(self, response):
        print("上游订单ID=" + self.oid)
        print("微店订单号=" + self.wd_order_no)
        print("支付宝URL=" + self.ali_url)
        url = "http://127.0.0.1:9501/wd/setInfo.do"
        headers = {'content-type': 'application/json'}
        requestData = {"id": self.oid, "order_id": self.wd_order_no, "url": self.ali_url}
        ret = requests.post(url, json=requestData, headers=headers)
        if ret.status_code == 200:
            text = json.loads(ret.text)
            print(text)




