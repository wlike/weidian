# -*- coding: utf-8 -*-
import scrapy
from scrapy.http import Request
import json


class WdsellerSendSpider(scrapy.Spider):
    name = 'wdseller_send'
    allowed_domains = ['wdseller']
    start_urls = ['http://sso.weidian.com/']

    custom_settings = {
        'DOWNLOADER_MIDDLEWARES': {
            'scrapy.downloadermiddlewares.useragent.UserAgentMiddleware': None,
            'wdscrapy.middle.sellerMiddleWares.RandomUserAgent': 400,
        }
    }

    def __init__(self, user=None, user_pass=None, orderid=None, expressno=None, *args, **kwargs):
        self.user = user
        self.user_pass = user_pass
        self.orderid = orderid
        self.expressno = expressno
        super(eval(self.__class__.__name__), self).__init__(*args, **kwargs)

    def start_requests(self):
        yield Request('https://d.weidian.com/weidian-pc/login/#/', callback=self.parse_with_cookie)

    def parse_with_cookie(self, response):
        pass